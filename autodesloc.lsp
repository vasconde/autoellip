;; AutoEllip
;;
;; Aplicacao em AutoLISP para desenhar elipses
;; a partir de um ficheiro de coordenadas (x,y)
;; e um ficheiro de parametros (ma-axis, mi-axis, angle)
;;
;; autor: Vasco Conde <www.vasconde.com>
;; data: 2012/09/25
;; versao: 0.5
;; (load "C:/Users/vasconde/Desktop/devel/autoellip/autodesloc.lsp")

;; Criacao da elipse
(defun desenha-ellipse (center maj-axis min-axis axis-angle)
  (entmake

   (list
    '(0 . "ELLIPSE")
    '(100 . "AcDbEntity")
    '(100 . "AcDbEllipse")
    (cons 10 center)
    (cons 11 (polar '(0 0) axis-angle (/ maj-axis 2.0)))
    (cons 40 (/ (float min-axis) maj-axis))

    )
   )
  )

;; Criacao do point
(defun desenha-point (x y)
  (entmake

   (list
    '(0 . "POINT")
    '(100 . "AcDbEntity")
    '(100 . "AcDbPoint")
    (cons 10 (list x y 0.0))
    (cons 62 3)      ; cor: 1 red, 2 amarelo, 3 verde
    (cons 370 100)  ; grossura em ceti mm

    )
   )
  )

;; Criacao da line
(defun desenha-line (x1 y1 x2 y2)
  (entmake

   (list
    '(0 . "LINE")
    '(100 . "AcDbEntity")
    '(100 . "AcDbLine")
    (cons 10 (list x1 y1 0.0))
    (cons 11 (list x2 y2 0.0))
    (cons 62 1)      ; cor: 1 red, 2 amarelo, 3 verde
    (cons 370 40)  ; grossura em ceti mm

    )
   )
  )

; retorna em radianos
(defun calcula-teta (dx dy / teta)

  (setq teta (atan dy dx))

  )

; recebe em radianos
(defun calcula-componentes (c teta)
  (list (* c (cos teta)) (* c (sin teta)) )
)

; t - angulo do desloc
; dt - +/- angulo do desloc
(defun setas-componentes (dx-i dy-i comp dteta / dx dy)
  ; inversao das componentes
  (setq dx (- dx-i))
  (setq dy (- dy-i))

  ; determinacao do teta
  (setq teta (calcula-teta dx dy))

  ; calcula as componentes para cada parte da seta
  (list (calcula-componentes comp (+ teta dteta))
	(calcula-componentes comp (- teta dteta)))

  )

;; Parsing das linhas de texto
;; cujo separador eh espaco e a linha comeca com uma string
;; E.G. PD    18983.3     34239.4   84732.8
(defun parse_nums (st / a k lst) ;; / significa variaveis locais
  (setq k 1)     ; iterador - avanca caracter a caracter
  (setq ek 1)    ; avanca elemento a elemento
  (setq a "")    ; acumulador de caracteres
  (setq lst nil) ; lista com os valores finais

  (repeat (strlen st) ; ciclo - repete numero de caracteres na linha vezes
	  (if (= (substr st k 1) " ") ; se a substring da pos k e tamanho 1 ==,
	      (if (/= (strlen a) 0) ; n eh um espaco seguido de um espaco
		  (progn
		    (if (/= ek 1) ; o primeiro elemento eh string e n double
			(setq lst (append lst (list (atof a)))) ; adiciona a ah lista
		      (setq lst (append lst (list a))))
		    (setq a "") ; reinicia o a
		    
		    (setq ek (+ ek 1))
		    )
		)
	    (setq a (strcat a (substr st k 1))) ; concata a com o char !=,
	    )
	  (setq k (+ k 1)) ; o iterador avanca
	  )

     ; necessario pq o ultimo char n eh ,
  (if (/= (strlen a) 0)
      (setq lst (append lst (list (atof a))))
    )
  lst
  )

(defun desenha-desloc (f-escala / f1 f2 dataline1 dataline2 scomp dteta)

  ;; informacao relativa as setas dos vetores
  (setq scomp 3.0) ; comprimento dos lados
  (setq dteta 30.0) ; angulo dos lados com o vetor

  ;; 1. DESENHA OS VERTICES DA REDE

  (setq f1 (open "c:/temp/coo.txt" "r")) ; carrega o ficheiro
  (setq f2 (open "c:/temp/desloc.txt" "r")) ; carrega o ficheiro

  (setq dataline1 (read-line f1)) ; le a primeira linha
  (setq dataline2 (read-line f2)) ; le a primeira linha

  (while (/= dataline1 nil)       ; se for diferente de nil (i.e. != EOF)

    (setq dataline1 (parse_nums dataline1)) ; faz o parsing
    (setq dataline2 (parse_nums dataline2)) ; faz o parsing

    (princ (nth 0 dataline1))
    (princ "\n")

    (desenha-point (nth 1 dataline1) (nth 2 dataline1)) ; desenha os pontos

    ; desenha os deslocamentos
    (desenha-line (nth 1 dataline1)
		  (nth 2 dataline1)
		  (+ (nth 1 dataline1) (* f-escala (nth 1 dataline2)))
		  (+ (nth 2 dataline1) (* f-escala (nth 2 dataline2))))

    ; desenha as setas
    (setq s-comps (setas-componentes (nth 1 dataline2) (nth 2 dataline2)
				     scomp (* dteta (/ pi 180)) ))

    (desenha-line (+ (nth 1 dataline1) (* f-escala (nth 1 dataline2)))
		  (+ (nth 2 dataline1) (* f-escala (nth 2 dataline2)))

		  (+ (+ (nth 1 dataline1) (* f-escala (nth 1 dataline2)))
		     (* f-escala (nth 0 (nth 0 s-comps)))
		     )

		  (+ (+ (nth 2 dataline1) (* f-escala (nth 2 dataline2)))
		     (* f-escala (nth 1 (nth 0 s-comps)))
		     ))

    (desenha-line (+ (nth 1 dataline1) (* f-escala (nth 1 dataline2)))
		  (+ (nth 2 dataline1) (* f-escala (nth 2 dataline2)))

		  (+ (+ (nth 1 dataline1) (* f-escala (nth 1 dataline2)))
		     (* f-escala (nth 0 (nth 1 s-comps)))
		     )

		  (+ (+ (nth 2 dataline1) (* f-escala (nth 2 dataline2)))
		     (* f-escala (nth 1 (nth 1 s-comps)))
		     ))


    (setq dataline1 (read-line f1)) ; le o ficheiro
    (setq dataline2 (read-line f2)) ; le o ficheiro
    )

  ;; 2. DESENHA OS DESLOCAMENTOS DA REDE

  

  ;(princ dataline1)
  )

;; programa principal
;;(defun c:autoellip ()
;;  (setq f (open "c:/acad/autoellip/data/coo.txt" "r")) ;-Opens the file to read (coordenadas)
;;  (setq g (open "c:/acad/autoellip/data/par.txt" "r")) ;-Opens the file to read (parametros)

;;  (setq dataline1 (read-line f))                ;-Reads the header
;;  (setq dataline2 (read-line g))                ;-Reads the header
;;
;;  (setq dataline1 (read-line f))                ;-Reads the 1st data line
;;  (setq dataline2 (read-line g))                ;-Reads the 1st data line
;;
;;  (setq gon2rad (/ pi 200.0))                   ;-conversor gon para rad
;;
;;  (setq e 2.0)                                  ;-ESCALA
;;
;;  (setq i 0)                                    ;-Iterador
;;
;;  (while (/= dataline1 "EOF")                   ;-Loop until the end of file
;;
;;    (setq dataline1 (parse_nums dataline1))
;;    (setq dataline2 (parse_nums dataline2))
;;
;;    (ellipsee dataline1 (* (nth 0 dataline2) e) (* (nth 1 dataline2) e) (* (- 100 (nth 2 dataline2)) gon2rad))
    
;;    (setq dataline1 (read-line f))              ;-Read the next data line
;;    (setq dataline2 (read-line g))              ;-Read the next data line
;;
;;    (setq i (+ i 1))                            ;-Incrementa o iterador
;;
;;    )          
;;  (close f)                                    ;-Close the file
;;  (close g)                                    ;-Close the file
;;
;;  ; imprime o numero de elipses desenhadas
;;  (princ i)
;;  (princ " elipses")
;;  (princ)
;;
;;  )


;; retorna em graus
;;(defun calcula-teta (dx dy / teta)
;;
;;  (setq teta (* (atan dy dx) (/ 180 pi)))
;;
;;  )
;;
;;; recebe em graus
;;(defun calcula-componentes (c teta)
;;  (setq teta (* teta (/ pi 180)))
;;  (list (* c (cos teta)) (* c (sin teta)) )
;;)
