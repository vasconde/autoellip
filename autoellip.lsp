;; AutoEllip
;;
;; Aplicacao em AutoLISP para desenhar elipses
;; a partir de um ficheiro de coordenadas (x,y)
;; e um ficheiro de parametros (ma-axis, mi-axis, angle)
;;
;; autor: Vasco Conde <www.vasconde.com>
;; data: 2012/09/25
;; versao: 0.5

;; Criacao da elipse
(defun ellipsee (center maj-axis min-axis axis-angle)
  (entmake
   (list
    '(0 . "ELLIPSE")
    '(100 . "AcDbEntity")
    '(100 . "AcDbEllipse")
    (cons 10 center)
    (cons 11 (polar '(0 0) axis-angle (/ maj-axis 2.0)))
    (cons 40 (/ (float min-axis) maj-axis))
    )
   )
  )

;; Parsing das linhas de texto
;; cujo separador eh a virgula
(defun parse_nums (st / a k lst)
  (setq k 1)
  (setq a "")
  (setq lst nil)
  (repeat (strlen st)
	  (if (= (substr st k 1) ",")
	      (progn
		(setq lst (append lst (list (atof a))))
		(setq a "")
		)
	    (setq a (strcat a (substr st k 1)))
	    )
	  (setq k (+ k 1))
	  )
  (setq lst (append lst (list (atof a))))
  )

;; programa principal
(defun c:autoellip ()
  (setq f (open "c:/acad/autoellip/data/coo.txt" "r")) ;-Opens the file to read (coordenadas)
  (setq g (open "c:/acad/autoellip/data/par.txt" "r")) ;-Opens the file to read (parametros)

  (setq dataline1 (read-line f))                ;-Reads the header
  (setq dataline2 (read-line g))                ;-Reads the header

  (setq dataline1 (read-line f))                ;-Reads the 1st data line
  (setq dataline2 (read-line g))                ;-Reads the 1st data line

  (setq gon2rad (/ pi 200.0))                   ;-conversor gon para rad

  (setq e 2.0)                                  ;-ESCALA

  (setq i 0)                                    ;-Iterador

  (while (/= dataline1 "EOF")                   ;-Loop until the end of file

    (setq dataline1 (parse_nums dataline1))
    (setq dataline2 (parse_nums dataline2))

    (ellipsee dataline1 (* (nth 0 dataline2) e) (* (nth 1 dataline2) e) (* (- 100 (nth 2 dataline2)) gon2rad))
    
    (setq dataline1 (read-line f))              ;-Read the next data line
    (setq dataline2 (read-line g))              ;-Read the next data line

    (setq i (+ i 1))                            ;-Incrementa o iterador

    )          
  (close f)                                    ;-Close the file
  (close g)                                    ;-Close the file

  ; imprime o numero de elipses desenhadas
  (princ i)
  (princ " elipses")
  (princ)

  )
